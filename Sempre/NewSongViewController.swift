//
//  newSongViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/17/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit



class NewSongViewController: UIViewController, NewPageDelegate {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var composerTextField: UITextField!
    @IBOutlet weak var beatsPerMeasureTextField: UITextField!
  
    @IBOutlet weak var beatsPerMinuteTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    var pages: [(lastMeasureNumber: Int, image: UIImage)] = []
    
    let context2 = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveSong(_ sender: Any) {
        let song = Song(context: context)
        song.beatsPerMinute = Int16(beatsPerMinuteTextField.text ?? "0") ?? 0
        song.beatsPerMeasure = Int16(beatsPerMeasureTextField.text ?? "0") ?? 0
        song.composer = composerTextField.text
        song.title = titleTextField.text
        //not needed:
        //self.performSegue(withIdentifier: "SAVEGoToWelcome" , sender: nil)
        //:not needed
       var measureNumbers: [Int] = []
        var images: [UIImage] = []
        
        for page in self.pages {
        
            measureNumbers.append(page.lastMeasureNumber)
            images.append(page.image)
        
        }
        
        song.pageMeasureNumbers = measureNumbers
        song.pageImages = images
        
        do{
            try context.save()
        } catch let error as NSError {
            print(error)
        
        }
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)

        
    }
    

    @IBAction func newPageTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toCameraView" , sender: nil)
    }
    
    func updateWithNewPage(page: (lastMeasureNumber: Int, image: UIImage)) {
        
        
        self.pages.append(page)
        self.tableView.reloadData()
        
        print("page count: " + String(pages.count))
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    
        if segue.identifier == "toCameraView" {
    
    
            let destinationVC = segue.destination as! CameraViewController
            destinationVC.delegate = self
    
    
        }
        
    
    
    }
    

    
}

extension NewSongViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(pages.count)
        return pages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = "Page " + String(indexPath.row+1)
        
        return cell
    }
    
    
}
